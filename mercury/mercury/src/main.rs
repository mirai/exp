/*
* The MIT License (MIT)

* Copyright (c) 2023-present Artemis Mirai <artemismirai@waifu.club>

* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
* OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

// -- imports --
use mercury::prelude::*;
use std::net::{
    Ipv4Addr,
    IpAddr,
};
use clap::crate_version;

// -- main --
#[tokio::main]
async fn main() -> MercuryResult<()> {
    let logger = Logger::new(true);
    logger.warn(format!("You are using Mercury `v{}`. This is an alpha build only intended for development purposes. Many things are buggy or just don't work. You have been warned.", crate_version!()));
    let conf = MercuryConf::from_file("Mercury.toml");
    let client_enabled = conf.client.unwrap_or_default().enabled;
    let server_enabled = conf.server.unwrap_or_default().enabled;

    if client_enabled && server_enabled {
        // Maybe one day, but I don't see a use case for this right now.
        logger.fatal("You cannot have the enabled fields of both the [client] and [server] set to true.");
    } else if client_enabled {
        // Start the `MercuryClient`.
        let mut client = MercuryClient::new(logger).await?;
        client.connect(
            std::env::var("TEST_HOST_IPV4")?
        ).await?;
    } else if server_enabled {
        // Start the `MercuryHost`.
        let host = MercuryHost::new(logger, false).await?;
        host.start(
            IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0))
        ).await?;
    }

    Ok(())
}
