/*
* The MIT License (MIT)

* Copyright (c) 2023-present Artemis Mirai <artemismirai@waifu.club>

* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
* OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

// -- imports --
use serde::{
    Serialize,
    Deserialize,
    self,
};

// -- message enum --
/// Highest level `Message` type for `Mercury`. This enum
/// (and the included structs) can be used for both `MercuryClient`
/// and `MercuryHost`! This is yet another benefit of packaging both
/// of these things together into one library rather than
/// having them fragmented.
#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
pub enum Message {
    HELLO(Hello),
    GOODBYE(Goodbye),
}

// -- greeting struct --
/// Base `HELLO` message structure. This isn't even close to being fully
/// implemented, the below struct just outlines an example struct.
#[derive(Serialize, Deserialize, Debug)]
pub struct Hello {
    pub kyber_pubkey: String,
}

// -- parting struct --
/// Base `GOODBYE` message structure. Same unimplemented notice as
/// the `Hello` struct should be taken into consideration.
#[derive(Serialize, Deserialize, Debug)]
pub struct Goodbye;
