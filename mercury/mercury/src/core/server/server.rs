/*
* The MIT License (MIT)

* Copyright (c) 2023-present Artemis Mirai <artemismirai@waifu.club>

* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
* OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

// -- imports --
use crate::prelude::*;
use std::net::IpAddr;
use tokio::{
    net::{
        TcpListener,
        TcpStream,
    },
    io::{
        AsyncReadExt,
        AsyncWriteExt,
        BufReader,
        AsyncBufReadExt,
    }
};
use serde::{
    Serialize,
    Deserialize,
};

// -- mercury host struct --
/// A Mercury seller P2P host. This cannot be implemented anonymously
/// until [Arti#693](https://gitlab.torproject.org/tpo/core/arti/-/issues/693) is solved.
pub struct MercuryHost {
    logger: Logger,
    hidden_service: bool,
}

// -- core impl --
impl MercuryHost {
    /// Creates a new `MercuryHost` object. Note that as of right now,
    /// this deanonymises the user due to a lack of hidden service
    /// support in `Arti`.
    pub async fn new(logger: Logger, hidden_service: bool) -> MercuryResult<Self> {
        // Fail if user attempts to host a hidden service enabled
        // `MercuryHost` as `Arti` doesn't support those yet.
        if hidden_service {
            logger.fatal("The underlying TOR routing library that Mercury relies on, Arti, does not yet support hosting hidden services.");
        }

        Ok(
            Self {
                logger,
                hidden_service,
            }
        )
    }

    /// Processes a TCP request stream. TODO(CRUCIAL): Fix TOR client support. For
    /// some odd reason, requests that aren't through Arti go through perfectly fine, but
    /// but request streams that are through Arti repeatedly send blank data packets.
    async fn process_stream(mut stream: TcpStream) -> MercuryResult<()> {
        let mut reader = BufReader::new(&mut stream);
        let mut message_raw = String::new();
        let mut complete = false;
        while !complete {
            let mut line = String::new();
            match reader.read_line(&mut line).await {
                Ok(_) => {
                    message_raw.push_str(&line);
                    if line.ends_with("\r\n") {
                        message_raw = message_raw.trim_end().into();
                        complete = true;
                    }
                },
                Err(_) => {
                    break;
                },
            }
        }
        println!("{}", message_raw);
        // First, let's read the whole request to a `String`.
        /*let mut message_str = String::new();
        stream.read_to_string(&mut message_str).await?;
        println!("{}", message_str);

        // Then, let's parse it as some variant of the `Message` enum.
        let message: Message = serde_json::from_str(&message_str)?;
        println!("{:#?}", message);*/
        Ok(())
    }

    /// Starts the `MercuryHost` TCP server on the specified
    /// `IpAddr`. Consumes `self`.
    pub async fn start(self, ip: IpAddr) -> MercuryResult<()> {
        self.logger.info(format!("Starting Mercury host on {}, port {}.", ip, MERCURY_P2P_PORT));
        let listener = TcpListener::bind(
            format!("{}:{}", ip, MERCURY_P2P_PORT)
        ).await?;
        self.logger.success("Successfully started Mercury host server!");

        loop {
            match listener.accept().await {
                Ok((mut stream, _)) => Self::process_stream(stream).await?,
                Err(e) => self.logger.warn(e.to_string()),
            };
        }

        //Ok(())
    }
}
