/*
* The MIT License (MIT)

* Copyright (c) 2023-present Artemis Mirai <artemismirai@waifu.club>

* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
* OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

// -- imports --
use crate::prelude::*;
use std::{
    net::{
        IpAddr,
        SocketAddr
    },
    fmt::Display,
    str::FromStr,
};
#[allow(unused_imports)]
use ::tokio::io::{
    AsyncReadExt,
    AsyncWriteExt,
};
use arti_client::*;
use tor_rtcompat::*;

// -- mercury client struct --
/// Base Mercury buyer client. Always routes through TOR for
/// buyer anonymity, though in the future I2P connections
/// may also be implemented.
#[allow(dead_code)]
pub struct MercuryClient<R: Runtime> {
    /// Internal TOR router - how we make connections anonymous.
    /// Generated when the `MercuryClient` object is constructed.
    _router: TorClient<R>,

    /// Interal connection datastream. Starts as `None`, but is
    /// set to a `MercuryConnection` when a connection with a host is
    /// established.
    _connection: Option<MercuryConnection>,

    /// Internal `Logger` object - used for writing fancy
    /// logs to stdout. Prettier alternative to just using
    /// `println!` and the like.
    logger: Logger,
}

// -- core impl --
impl MercuryClient<PreferredRuntime> {
    /// Creates a new `MercuryClient` object with the user's passed
    /// `Logger` object.
    pub async fn new(logger: Logger) -> MercuryResult<Self> {
        // https://docs.rs/arti-client/0.9.0/arti_client/index.html#connecting-to-tor
        logger.info("Creating a new TOR router...");
        let _router = TorClient::create_bootstrapped(
            TorClientConfig::default()
        ).await?;
        logger.success("Successfully created a TOR router!");

        let _connection = None;

        Ok(
            Self {
                _router,
                _connection,
                logger,
            }
        )
    }

    /// Internal function that creates a `MercuryConnection` with a `MercuryHost`.
    /// Panics if `self._connection` is `Some(_)` - multiple connections
    /// cannot exist at the same time in a single client.
    async fn _open_connection(&mut self, addr: SocketAddr, hidden_service: bool) -> MercuryResult<()> {
        // Fail if client has an open connection - we don't have the luxury of
        // parallel client connections yet.
        if let Some(_) = self._connection {
            // `Logger::fatal` panics, so `else { ... }` blocks aren't required as
            // they're implied.
            self.logger.fatal("This client already has an open connection stream!");
        }

        if hidden_service {
            self.logger.fatal("The underlying TOR routing library that Mercury relies on, Arti, does not yet support connecting to hidden services.");
        }

        self._connection = Some(
            MercuryConnection::new(addr, hidden_service)
        );

        Ok(())
    }

    /// Internal function that closes an existing connection. Panics
    /// if `self._connection` is None - you can't close a
    /// connection that doesn't exist!
    async fn _close_connection(&mut self) -> MercuryResult<()> {
        if let Some(ref mut connection) = self._connection {
            connection.goodbye(&mut self._router).await?;
            self._connection = None;
        } else {
            self.logger.fatal("This client has no open connection streams!");
        }

        Ok(())
    }

    /// Creates a new `MercuryConnection`.
    pub async fn connect<S: Display>(&mut self, host: S) -> MercuryResult<()> {
        self.logger.info(format!("Connecting to {}...", &host));
        let ip = IpAddr::from_str(&host.to_string())?;

        let addr = (ip, MERCURY_P2P_PORT).into();
        self._open_connection(addr, false).await?;
        println!("successfully called Self::_open_connection!");
        if let Some(ref mut connection) = self._connection {
            self.logger.info("Sending client HELLO...");
            let resp = connection.hello(&mut self._router).await?;
            self.logger.info(format!("Sent client HELLO! Received server HELLO: {:#?}", resp));
        }
        self.logger.success(format!("Opened a connection to {}!", &host));

        Ok(())
    }

    /// Closes the current `MercuryConnection`.
    pub async fn disconnect(&mut self) -> MercuryResult<()> {
        self.logger.info("Closing the current connection...");
        self._close_connection().await?;
        self.logger.success("Closed the current connection!");
        Ok(())
    }
}
