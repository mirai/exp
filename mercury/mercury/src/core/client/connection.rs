/*
* The MIT License (MIT)

* Copyright (c) 2023-present Artemis Mirai <artemismirai@waifu.club>

* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
* OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

// -- imports --
use crate::prelude::*;
use tokio::io::{
    AsyncReadExt,
    AsyncWriteExt,
};
use std::net::SocketAddr;
use arti_client::{
    TorAddr,
    TorClient,
    DangerouslyIntoTorAddr
};
use tor_rtcompat::PreferredRuntime;

// -- mercury connection struct --
/// Represents a TCP connection with a `MercuryHost`.
pub struct MercuryConnection {
    pub addr: SocketAddr,
    pub host_pubkey: Option<String>,
    pub hidden_service: bool,
}

// -- core impl --
impl MercuryConnection {
    /// Creates a new `MercuryConnection` object from a host `IpAddr`, a `DataStream`
    /// object, and a `hidden_service` bool. the host `IpAddr` will likely change
    /// to a `SocketAddr` when hidden service connectivity is successfully implemented.
    pub fn new(addr: SocketAddr, hidden_service: bool) -> Self {
        Self {
            addr,
            host_pubkey: None,
            hidden_service,
        }
    }

    async fn _send(&self, router: &mut TorClient<PreferredRuntime>, message: Message, _encrypted: bool) -> MercuryResult<String> {
        let mut resp = String::new();
        let mut sock = router.connect(
            self.addr.into_tor_addr_dangerously()?
        ).await?;

        sock.write_all(
            (
                r#"{"kyber_pubkey": "__dev_pubkey__"}"#
                .to_string()
                    + "\r\n"
            )
            /*serde_json::to_string(
                &message
            )?*/.as_bytes()
        ).await?;
        sock.flush().await?;
        sock.shutdown().await?;
        /*sock.read_to_string(&mut resp).await?;*/
        Ok(resp)
    }

    /// "Hello" message to `MercuryHost`.
    pub async fn hello(&self, router: &mut TorClient<PreferredRuntime>) -> MercuryResult<String> {
        let msg = Message::HELLO(
            Hello {
                kyber_pubkey: "__dev_pubkey__".into()
            }
        );
        let resp = self._send(router, msg, false).await?;
        Ok(resp)
    }

    /// "Goodbye" message to `MiraiHost`.
    pub async fn goodbye(&self, _router: &mut TorClient<PreferredRuntime>) -> MercuryResult<()> {
        Ok(())
    }
}
