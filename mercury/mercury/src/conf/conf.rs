/*
* The MIT License (MIT)

* Copyright (c) 2023-present Artemis Mirai <artemismirai@waifu.club>

* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
* OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

// -- imports --
use std::{
    fmt::Display,
    fs,
    default::Default,
};
use serde::{
    Serialize,
    Deserialize,
};

// -- config structs --
/// Base server configuration.
#[derive(Serialize, Deserialize, Debug)]
pub struct ServerConf {
    pub enabled: bool,
    pub hidden_service: Option<bool>,
}

/// Base client configuration.
#[derive(Serialize, Deserialize, Debug)]
pub struct ClientConf {
    pub enabled: bool,
    pub tor_routing: Option<bool>,
}

/// Base `Mercury.toml` configuration. Note that the
/// `enabled` variables for these fields are mutually
/// exclusive, so you cannot have both the `[client].enabled`
/// and `[server].enabled` fields set to true.
#[derive(Serialize, Deserialize, Debug)]
pub struct MercuryConf {
    /// Server config.
    pub server: Option<ServerConf>,

    /// Client config.
    pub client: Option<ClientConf>,
}

// -- mercury config impl --
impl MercuryConf {
    pub fn from_file<S: Display>(file_name: S) -> Self {
        let config_str = fs::read_to_string(file_name.to_string()).unwrap();
        toml::from_str(&config_str).unwrap()
    }
}

// -- default trait impls (for `Option::unwrap_or_default`) --
impl Default for ClientConf {
    fn default() -> Self {
        Self {
            enabled: false,
            tor_routing: None,
        }
    }
}

impl Default for ServerConf {
    fn default() -> Self {
        Self {
            enabled: false,
            hidden_service: None,
        }
    }
}
