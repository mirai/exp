# Mercury Roadmap
From top to bottom, these items are listed in chronological order.
## Stage I - *"It works!"*
When all of these are completed, Mercury's core functionality will be fully implemented, but both security and usability will still be a little iffy - wait until *at least* Stage II for any real-world use.
- [ ] Basic TCP APIs were implemented for both `MercuryClient` and `MercuryHost`.
- [ ] TCP stream encryption with [Kyber](https://github.com/Argyle-Software/kyber) was implemented.
- [ ] Base Monero transaction processing was implemented.
- [ ] Scam prevention mechanisms akin to escrow were added (look into Monero MultiSig and Atomic Swap research.)

## Stage II - *"It's safe to use in a production environment!"*
When all of these are completed, Mercury's core functionality will be implemented, as well as more security mechanisms.
- [ ] A decentralised reputation system was added (maybe draw inspiration from [aeternity's GOSSIP implementation](https://github.com/aeternity/protocol/blob/master/GOSSIP.md)?)
- [ ] A seller discovery system was implemented ([Ref. NeroShop#6](https://github.com/larteyoh/testshop/issues/6).)
- [ ] Hidden service connectivity was successfully implemented upstream ([Arti#693](https://gitlab.torproject.org/tpo/core/arti/-/issues/693)) and here.

## Stage III - *"It's nearly perfect!"*
When all of these are completed, Mercury should be *amazing* for end-users, and buyers shouldn't have to touch the CLI once (sellers will probably have to touch it like once.) This is the "user friendliness" stage.
- [ ] Mercury can now make multiple connections at once.
- [ ] Mercury now makes use of [succinct data structures](https://en.wikipedia.org/wiki/Succinct_data_structure).
- [ ] A basic search mechanism was implemented ([Ref. NeroShop#13](https://github.com/larteyoh/testshop/issues/13).)
- [ ] A WebUI was implemented (HTML only - no JS!).
- [ ] I2P routing was implemented (optional milestone.)
