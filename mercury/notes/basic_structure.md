# Basic Structure
The basic structure of Mercury is as follows:
- A seller starts a `MercuryHost` server ("node?" terminology is unclear at the moment, will depend on implementation specifics), then sends the address to their buyer.
- The buyer then starts their `MercuryClient` client, inputs the given address, and the transaction commences.
