# Mercury
An experimental demo of a fully decentralised online marketplace 💵

---

Mercury is an attempt at creating a *fully* decentralised and P2P marketplace. Mercury's goal is to provide a private, secure, and safe transactional layer between buyers and sellers. Check out the [basic structure](./notes/basic_structure.md) to get a better understanding of the protocol itself.

## Security
To be sure you're downloading an unmodified version of Mercury, check that:
- All file changes have an associated Git commit in the history
- All Git commits are signed with the following PGP key: [artemismirai.pub.asc](./pgp/artemismirai.pub.asc) (keyid `0x7D04E4915F2181D8`)

If all of this is correct, you should be safe.

## Status
Mercury is in absolutely *no* state for production use yet - we're pre-stage I. Check out the [roadmap](./notes/roadmap.md) for an overview of what we still need to do and implement prior to production.

## Credits, Inspiration
- [layter](https://github.com/larteyoh): I've taken some ideas from NeroShop that I wouldn't have otherwise come up with and put them on my roadmap

## Similar Projects
[This has been moved.](./notes/similar_projects.md)

---

Developed with <3 by [Mirai](https://git.disroot.org/mirai).

![Mirai organisation logo](https://git.disroot.org/avatars/8c879ce5f27a376a3f79b11a4e59c9fcb622d43de8a08dde39333aecf673ac9c)

みらい • ˶ᵔ ᵕ ᵔ˶ • Mirai
