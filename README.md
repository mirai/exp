# exp

Mirai's pre-release, experimental software 🧪

---

This repository holds applications that are in the alpha phase. The goal with this repository is for high-demand alpha projects to gain enough support and development to mature and eventually move to their own repositories.

Right now, we have a few projects in the works:
- Mercury: An experimental demo of a fully decentralised online marketplace
- KDT: [This was moved!](https://git.disroot.org/mirai/kdt)

## Contributing
If you'd like to contribute, feel free to do so! You can open a pull request in this repository, or if you don't wish to create an account here, you can just email me the proposed code changes at artemismirai@waifu.club and I'll merge them manually.

## Licensing
As these projects are works in progress, I won't put usage restrictions on them while they're in this repository. However, once a project that was here moves to its own repository, this statement is voided by the license chosen for that particular project.

---

Developed with <3 by [Mirai](https://git.disroot.org/mirai).

![Mirai organisation logo](https://git.disroot.org/avatars/8c879ce5f27a376a3f79b11a4e59c9fcb622d43de8a08dde39333aecf673ac9c)

みらい • ˶ᵔ ᵕ ᵔ˶ • Mirai
